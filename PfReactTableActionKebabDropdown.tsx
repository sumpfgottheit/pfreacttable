/* tslint:disable:jsx-no-lambda */
import React from 'react';
import {Dropdown, DropdownItem, KebabToggle} from '@patternfly/react-core';

interface IKebabDropDownState {
    isOpen: boolean
}

class PfReactTableActionKebabDropdown extends React.Component<{ actions: any[], entry: any[] }, IKebabDropDownState> {
    public onToggle: (isOpen: any) => void;
    public onSelect: (event) => void;

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false
        };
        this.onToggle = isOpen => {
            this.setState({
                isOpen
            });
        };
        this.onSelect = event => {
            event.preventDefault();
            this.setState({
                isOpen: !this.state.isOpen
            });
        };
    }


    public render() {
        const {isOpen} = this.state;
        const dropdownItems = this.props.actions.map((action, index) => {
            return (
                <DropdownItem key={index} component="button" onClick={() => action[1](this.props.entry)}>
                    {action[0]}
                </DropdownItem>)
        });
        return (
            <Dropdown
                onSelect={this.onSelect}
                toggle={<KebabToggle onToggle={this.onToggle}/>}
                isOpen={isOpen}
                isPlain={true}
                dropdownItems={dropdownItems}
            />
        );
    }
}

export {PfReactTableActionKebabDropdown};
