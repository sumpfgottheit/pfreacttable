import {PfReactTableColumn} from "./PfReactTableColumn";


interface IPfReactPaginationTable {
    actions: any[];
    columns: PfReactTableColumn[];

    fetch(page: number, itemsPerPage: number, sortLabel: string, sortAsc: boolean);
}

export {IPfReactPaginationTable};