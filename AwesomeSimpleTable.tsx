import * as React from 'react';
import {PfReactPaginationTable} from './PfReactPaginationTable';
import {PfReactTableColumn} from './PfReactTableColumn';

interface IAweSomeTable {

}

interface IAwesomeSimpleTableProps {
    columns: PfReactTableColumn[];
    rows: any[],
    sortAndPaginationHandler?: any
    sortFunction?: any
}

interface IAwesomeSimpleTableState {
    sortColumnName: string,
    sortAsc: boolean,
    loading: boolean,
    page: number,
    rows: any[],
    itemsPerPage: number,
    totalItems: number
}


class AwesomeSimpleTable extends React.Component<IAwesomeSimpleTableProps, IAwesomeSimpleTableState> implements IAweSomeTable {

    constructor(props) {
        super(props);
        this.state = {
            itemsPerPage: 10,
            loading: true,
            page: 1,
            rows: [],
            sortAsc: true,
            sortColumnName: '',
            totalItems: this.props.rows.length
        };
        this.computeRows = this.computeRows.bind(this);
        this.sortAndPaginationHandler = this.sortAndPaginationHandler.bind(this);
    }

    public sortFunction(sortColumnIndex, a, b) {
        return a[sortColumnIndex] - b[sortColumnIndex]
    }

    public computeRows(page: number, itemsPerPage: number, sortColumnName: string, sortAsc: boolean): any[] {

        const totalItems = this.props.rows ? this.props.rows.length : 0;

        const sortColumnIndex = this.props.columns.findIndex((column) => column.name === sortColumnName);
        if (sortColumnIndex === -1) {
            console.log(`Column with name ${sortColumnName} not found`);
        }

        const newRows = this.props.rows.slice();
        if (this.props.hasOwnProperty('sortFunction')) {
            newRows.sort((a, b) => this.props.sortFunction(sortColumnIndex, a, b));
        } else {
            newRows.sort((a, b) => this.sortFunction(sortColumnIndex, a, b));
        }

        if (!sortAsc) {
            newRows.reverse();
        }

        const pages = totalItems === 0 ? 0 : Math.ceil(totalItems / itemsPerPage);
        if (pages < page) {
            page = pages
        }

        const firstItem = (itemsPerPage * page) - itemsPerPage;
        let lastItem = firstItem + itemsPerPage;
        if (lastItem > totalItems) {
            lastItem = totalItems
        }

        return newRows.slice(firstItem, lastItem);
    }

    public sortAndPaginationHandler(page: number, itemsPerPage: number, sortColumnName: string, sortAsc: boolean) {
        if (this.props.hasOwnProperty('sortAndPaginationHandler')) {
            this.props.sortAndPaginationHandler(page, itemsPerPage, sortColumnName, sortAsc)
        } else {
            this.setState({
                itemsPerPage,
                loading: false,
                page,
                sortAsc,
                sortColumnName,
            });
        }

    }

    public render() {
        const {page, itemsPerPage, sortColumnName, sortAsc} = this.state;
        const rows = this.computeRows(page, itemsPerPage, sortColumnName, sortAsc);
        const totalItems = this.props.rows.length;
        return (
            <PfReactPaginationTable
                columns={this.props.columns}
                rows={rows}
                itemsPerPage={itemsPerPage}
                loading={false}
                page={page}
                sortAsc={sortAsc}
                sortColumnName={sortColumnName}
                totalItems={totalItems}
                onSortAndPagination={this.sortAndPaginationHandler}
            />
        )
    }
}

export {
    AwesomeSimpleTable
};
