import React from 'react'
import {LongArrowAltUpIcon, LongArrowAltDownIcon, ArrowsAltVIcon} from '@patternfly/react-icons';
import './pfreacttable.css';

const TableHeaderCell = ({column, sortInfo}) => {
    const {name, label, sortable,} = column;
    const sorted = name === sortInfo.columnName;
    if (!sortable) {
        return <th>{label}</th>
    } else {
        let additionalProps = {};
        if (typeof sortInfo.handler === 'function') {
            additionalProps = {onClick: (e) => sortInfo.handler(name)}
        }
        if (!sorted) {
            return (
                <th
                    className="pf-c-table__sort  "
                    aria-sort="none"
                    scope="col"
                    {...additionalProps}
                >
                    <button className="pf-c-button pf-m-plain" type="button">
                        {label}
                        <span className="pf-c-table__sort-indicator">
              <ArrowsAltVIcon/>
          </span>
                    </button>
                </th>)
        } else {
            const SortIcon = sortInfo.asc ? LongArrowAltUpIcon : LongArrowAltDownIcon;
            return (
                // @ts-ignore
                <th
                    className="pf-c-table__sort  pf-m-selected "
                    aria-sort="ascending"
                    scope="col"
                    {...additionalProps}
                >
                    <button className="pf-c-button pf-m-plain" type="button">
                        {label}
                        <span className="pf-c-table__sort-indicator">
                <SortIcon/>
              </span>
                    </button>
                </th>
            )
        }
    }
};

/*
 * Header Row
 */
const TableHeaderRow = ({columns, sortInfo}) => {
    const content = columns.map((column, index) =>
        <TableHeaderCell
            column={column}
            key={index}
            sortInfo={sortInfo}
        />);
    return (
        <tr>{content}</tr>
    )
};

const TableBodyCell = ({content}) => {
    return <td>{content}</td>
};

const TableBodyRow = ({row}) => {
    const content = row.map((cell, index) =>
        <TableBodyCell
            content={cell}
            key={index}/>);
    return (
        <tr>
            {content}
        </tr>
    )
};

const TableBody = ({rows}) => {
    return rows.map((row, index) => <TableBodyRow row={row} key={index}/>)
};

const PfReactTable = ({columns, rows, sortInfo}) => {
    return (
        <table className="pf-c-table pf-m-compact pf-m-grid-md pfsaftable" role="grid" aria-label="This is a simple table example" id="simple-table">
            <thead>
            <TableHeaderRow columns={columns} sortInfo={sortInfo}/>
            </thead>
            <tbody>
            <TableBody rows={rows}/>
            </tbody>
        </table>
    )
};

export {PfReactTable};
