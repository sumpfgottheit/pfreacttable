class PfReactTableColumn {
    public name: string;
    public label: string;
    public sortable: boolean;
    public renderFunction: any;

    constructor(name: string, label: string | null = null, sortable: boolean = false, renderFunction?: (value) => string|any) {
        this.name = name;
        this.label = label === null ? name : label;
        this.sortable = sortable;
        this.renderFunction = renderFunction
    }

    public render(value) {
        return (typeof this.renderFunction === 'undefined') ? value : this.renderFunction(value)
    }
}


export {PfReactTableColumn};