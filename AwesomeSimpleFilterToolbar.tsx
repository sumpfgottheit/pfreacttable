import React from 'react';
import {
    DataToolbar,
    DataToolbarItem,
    DataToolbarContent,
    DataToolbarToggleGroup,
} from '@patternfly/react-core/dist/esm/experimental';
import {Button, ButtonVariant, InputGroup, TextInput} from '@patternfly/react-core';
import {FilterIcon, EraserIcon} from '@patternfly/react-icons'


interface IAwesomeSimpleFilterToolbarProps {
    onFilterChange: any,
    filterValue: string,
    placeholder: string
}

class AwesomeSimpleFilterToolbar extends React.Component<IAwesomeSimpleFilterToolbarProps> {
    public onInputChange: (newValue: any) => void;

    constructor(props) {
        super(props);

        this.onInputChange = (newValue) => {
            this.props.onFilterChange(newValue)
        };
        this.handleButtonClearClick = this.handleButtonClearClick.bind(this);
    }

    public handleButtonClearClick() {
        this.props.onFilterChange('')
    }

    public render() {
        const toggleGroupItems = <React.Fragment>
            <DataToolbarItem>
                <InputGroup>
                    <TextInput
                        name="filter1"
                        id="filter1"
                        type="search"
                        placeholder={this.props.placeholder}
                        aria-label="Filter Input"
                        onChange={this.onInputChange}
                        value={this.props.filterValue}/>
                    <Button
                        variant={ButtonVariant.control}
                        isDisabled={this.props.filterValue.length === 0}
                        aria-label="Clear Input"
                        onClick={this.handleButtonClearClick}
                    >
                        <EraserIcon/>
                    </Button>
                </InputGroup>
            </DataToolbarItem>
        </React.Fragment>;

        const items = <DataToolbarToggleGroup toggleIcon={<FilterIcon/>} breakpoint='xl'>{toggleGroupItems}</DataToolbarToggleGroup>;

        return <DataToolbar id="data-toolbar-component-managed-toggle-groups" className='pf-m-toggle-group-container'>
            <DataToolbarContent>
                {items}
            </DataToolbarContent>
        </DataToolbar>;
    }
}

export {AwesomeSimpleFilterToolbar}