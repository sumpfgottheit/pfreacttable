import React from 'react';
import {PageSection, PageSectionVariants, Pagination, Title} from '@patternfly/react-core';
import {PfReactTable} from "./PfReactTable";
import {PfReactTableColumn} from "./PfReactTableColumn";

interface IPfReactPaginationTableState {
    error: string,
}

interface IPfReactPaginationTableProps {
    sortColumnName: string,
    sortAsc: boolean,
    rows: any[],
    columns: PfReactTableColumn[];
    loading: boolean
    page: number,
    totalItems: number,
    itemsPerPage: number,

    onSortAndPagination(page: number, itemsPerPage: number, sortColumName: string, sortAsc: boolean);
}

class PfReactPaginationTable extends React.Component<IPfReactPaginationTableProps, IPfReactPaginationTableState> {
    constructor(props) {
        super(props);
        this.state = {
            error: '',
        };

        this.onPerPageSelect = this.onPerPageSelect.bind(this);
        this.onSetPage = this.onSetPage.bind(this);
        this.sortHandler = this.sortHandler.bind(this);
    }

    public onPerPageSelect(evt, value) {
        const {page, sortColumnName, sortAsc} = this.props;
        return this.props.onSortAndPagination(page, value, sortColumnName, sortAsc)
    };

    public onSetPage(evt, value) {
        const {itemsPerPage, sortColumnName, sortAsc} = this.props;
        return this.props.onSortAndPagination(value, itemsPerPage, sortColumnName, sortAsc)
    };

    public sortHandler(columnName) {
        const {itemsPerPage, sortColumnName, sortAsc} = this.props;
        if (sortColumnName === columnName) {
            this.props.onSortAndPagination(1, itemsPerPage, columnName, !sortAsc);
        } else {
            this.props.onSortAndPagination(1, itemsPerPage, columnName, true);
        }
    }


    public componentDidMount() {
        const {itemsPerPage, sortColumnName, sortAsc} = this.props;
        this.props.onSortAndPagination(1, itemsPerPage, sortColumnName, sortAsc)
    }

    public renderPagination() {
        const {page, itemsPerPage, totalItems} = this.props;
        return (
            <Pagination
                itemCount={totalItems}
                page={page}
                perPage={itemsPerPage}
                onSetPage={this.onSetPage}
                onPerPageSelect={this.onPerPageSelect}
                variant='bottom'
            />
        );
    }

    public render() {
        const {loading, rows, columns, sortColumnName, sortAsc} = this.props;
        const sortInfo = {columnName: sortColumnName, asc: sortAsc, handler: this.sortHandler};
        return (
            <React.Fragment>
                {this.renderPagination()}
                {!loading && (
                    <PfReactTable
                        columns={columns}
                        rows={rows}
                        sortInfo={sortInfo}
                    />
                )}
                {loading && <Title size="3xl">Please wait while loading data</Title>}
            </React.Fragment>
        );
    }
}

export {PfReactPaginationTable};

